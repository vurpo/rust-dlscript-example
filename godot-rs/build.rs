extern crate bindgen;

use std::env;
use std::path::Path;

fn main() {
  let out_dir = env::var("OUT_DIR").unwrap();
  bindgen::builder()
    .header("godot/modules/dlscript/godot_core_api.h")
    .generate().unwrap()
    .write_to_file(Path::new(&out_dir).join("godot_core_api.rs")).unwrap();
}
