#![feature(proc_macro)]

#![allow(unused_variables)]

extern crate godot_macros;

pub mod godot_core_api;
pub mod core_types;
pub use core_types::*;

use std::ffi::{
  CString,
};
use std::panic::catch_unwind;
use std::boxed::Box;

pub use godot_macros::godot_export;

#[allow(non_camel_case_types)]
type void = std::os::raw::c_void;

pub trait GodotObject where Self: std::marker::Sized + std::panic::RefUnwindSafe, Self: GodotObjectRegisterMethods {
  unsafe extern "C" fn _new(p_instance: *mut godot_core_api::godot_object) -> *mut void {
    if let Ok(r) = catch_unwind(|| {
      Box::into_raw(Box::new(Self::new())) as *mut void
    }) {
      r
    } else {
      println!("new panicked! ending the process");
      std::process::exit(1);
    }
  }
  fn new() -> Self;

  unsafe extern "C" fn _destroy(p_instance: *mut godot_core_api::godot_object, p_data: *mut void) {
    let ref mut this = *(p_data as *mut Self);
    if let Err(_) = catch_unwind(|| {
      this.destroy();
    }) {
      println!("destroy panicked! ending the process");
      std::process::exit(1);
    }
  }
  fn destroy(&self) {}
}

pub trait GodotObjectRegisterMethods {
  fn register_methods();
}

pub unsafe fn register_godot_class<T: GodotObject>(class_name: &str) -> Result<(),()> {
  godot_core_api::godot_script_register(CString::new(class_name).unwrap().as_ptr(), CString::new("Node2D").unwrap().as_ptr(), Some(T::_new), Some(T::_destroy));
  Ok(())
}

#[macro_export]
macro_rules! generate_dlscript_init {
  ( $($t:ident),* ) => {
    #[no_mangle]
    pub unsafe extern "C" fn godot_dlscript_init() {
      use godot::GodotObjectRegisterMethods;
      println!("This is a generated godot_dlscript_init function");
      $(
        println!(" - registering class {}", stringify!($t));
        godot::register_godot_class::<$t>(stringify!($t)).unwrap();
        $t::register_methods();
      )*
      println!("all classes and methods registered");
    }
  }
}
