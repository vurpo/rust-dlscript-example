use godot_core_api;
use std;

impl From<bool> for godot_core_api::godot_variant {
  fn from(i: bool) -> godot_core_api::godot_variant {
    unsafe{
      let mut ret: godot_core_api::godot_variant = std::mem::uninitialized();
      godot_core_api::godot_variant_new(&mut ret as *mut _); 
      ret
    } //TODO: actually create an actual variant object
  }
}
impl From<i32> for godot_core_api::godot_variant {
  fn from(i: i32) -> godot_core_api::godot_variant {
    unsafe{
      let mut ret: godot_core_api::godot_variant = std::mem::uninitialized();
      godot_core_api::godot_variant_new_int(&mut ret as *mut _, &i as *const _); 
      ret
    }
  }
}
impl From<String> for godot_core_api::godot_variant {
  fn from(i: String) -> godot_core_api::godot_variant {
    unsafe{
      let mut ret: godot_core_api::godot_variant = std::mem::uninitialized();
      godot_core_api::godot_variant_new(&mut ret as *mut _); 
      ret
    } //TODO: actually create an actual variant object
  }
}
impl<'a> From<&'a str> for godot_core_api::godot_variant {
  fn from(i: &'a str) -> godot_core_api::godot_variant {
    unsafe{
      let mut ret: godot_core_api::godot_variant = std::mem::uninitialized();
      godot_core_api::godot_variant_new(&mut ret as *mut _); 
      ret
    } //TODO: actually create an actual variant object
  }
}

impl From<()> for godot_core_api::godot_variant {
  fn from(i: ()) -> godot_core_api::godot_variant {
    unsafe {
      let mut ret: godot_core_api::godot_variant = std::mem::uninitialized();
      godot_core_api::godot_variant_new(&mut ret as *mut _); //this feels stupid
      ret
    }
  }
}

impl Into<i32> for godot_core_api::godot_variant {
  fn into(self) -> i32 {
    unsafe {
      let mut ret: i32 = std::mem::uninitialized();
      godot_core_api::godot_variant_get_int(&self as *const _, &mut ret as *mut _);
      ret
    }
  }
}
