#![feature(proc_macro)]
#[macro_use]
extern crate godot;
use godot::godot_export;
use godot::GodotObject;

struct MyObject{
  x: i32,
}
impl GodotObject for MyObject {
  fn new() -> MyObject {
    println!("new");
    MyObject{
      x: 10
    }
  }
}
#[godot_export]
impl MyObject {
  fn _notification(&self, what: i32) {
    if what == 13 {
      println!("ready!");
    }
  }

  pub fn other(&self, this: i32, that: i32) -> i32 {
    println!("Other called!");
    this+that
  }
}

struct MyOtherObject{
  x: i32,
}
impl GodotObject for MyOtherObject {
  fn new() -> MyOtherObject {
    MyOtherObject {
      x: 0,
    }
  }
}
#[godot_export]
impl MyOtherObject {

  pub fn increment(&mut self) {
    self.x += 1;
  }

  pub fn get_value(&self) -> i32 {
    self.x
  }
  
  pub fn takes_arguments(&self, x: i32, y: i32) {
  }

  pub fn static_fn(x: i32, y: i32) {
  }

  fn dont_export(&self) {
  }
}

generate_dlscript_init!{
  MyObject,
  MyOtherObject
}
